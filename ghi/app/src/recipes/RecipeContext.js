import { createContext, useContext, useState} from 'react';

const RecipeContext = createContext();

export function useRecipeContext() {
    return useContext(RecipeContext);
}

export function RecipeProvider({ children }) {
    const [recipes, setRecipes] = useState([]);

    const updateRecipeName = (recipeId, newName) => {
        setRecipes(prevRecipes => {
            return prevRecipes.map(recipe => {
                if (recipe.id ===recipeId) {
                    return { ...recipe, name: newName };
                }
                return recipe
            });
        });
    };
    const fetchRecipes = async () => {
        const url = "http://localhost:8000/api/recipes/";
        const response = await fetch(url);
        if (response.ok) {
            const { recipes } = await response.json();
            setRecipes(recipes);
        }
    };
    return (
        <RecipeContext.Provider value={{ recipes, updateRecipeName, fetchRecipes }}>
            {children}
        </RecipeContext.Provider>
    );
}
