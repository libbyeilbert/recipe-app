import Nav from './Nav';
import RecipeView from "./recipes/RecipeView";
import Recipe from "./recipes/Recipe";
import RecipeList from "./recipes/RecipeList";
import IngredientCost from "./ingredients/IngredientCost";
import NewRecipeForm from "./recipes/NewRecipeForm";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useEffect, useState } from "react";
import styles from './index.module.css';
import { RecipeProvider } from './recipes/RecipeContext';


function App() {
  const [recipes, setRecipes] = useState([]);
  const [ingredients, setIngredients] = useState([]);

  const fetchRecipes = async () => {
    const url = "http://localhost:8000/api/recipes/";
    const response = await fetch(url);
    if (response.ok) {
      const { recipes } = await response.json();
      setRecipes(recipes);
    }
  };

  const fetchIngredients = async () => {
    const url = "http://localhost:8000/api/recipes/ingredients/";
    const response = await fetch(url);
    if (response.ok) {
      const { ingredients } = await response.json();
      setIngredients(ingredients);
    }
  };

  useEffect(() => {
    fetchRecipes();
    fetchIngredients();
  }, []);
  return (
    <RecipeProvider>
    <BrowserRouter>
    <Nav />
      <div className={styles.content}>
        <Routes>
          <Route path="/ingredients/">
            <Route path=":id" element={<IngredientCost />} />
          </Route>
          <Route path="/recipes/">
            <Route index element={<RecipeList recipes={recipes} />} />
            <Route path="new" element={<NewRecipeForm fetchRecipes={fetchRecipes} />} />
            <Route path=":id" element={<Recipe recipes={recipes} fetchRecipes={fetchRecipes} />} />
            <Route path=":id/view" element={<RecipeView fetchRecipes={fetchRecipes}/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
    </RecipeProvider>
  )
};

export default App;
